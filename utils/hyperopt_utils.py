import sys
sys.path.append("..")
import time

from pytext.builtin_task import add_include
from pytext.main import pytext_config_from_json, train_model
add_include("custom_rnng")

from copy import deepcopy
from io import StringIO
import sys
import matplotlib.pyplot as plt

from IPython import display
import os
from hyperopt import tpe, Trials, fmin, space_eval
from hyperopt import STATUS_OK
import pickle
import numpy as np




class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self
    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio    # free up some memory
        sys.stdout = self._stdout
        
# class Struct:
#     def __init__(self, **entries):
#         self.__dict__.update(entries)

# def parse_dict(dic):
#     new_dict = {}
#     for k, v in dic.items():
#         if type(v) == dict:
#             v = parse_dict(v)
#         new_dict[k.split('__')[0]] = v
            
#     return new_dict



def modify_param(dic, new_param):
    new_dict = {}
    for k, v in dic.items():
        if type(v) == dict:
            v = modify_param(v, new_param)
        
        if k in new_param.keys():
            new_dict[k] = new_param[k]
        else:
            new_dict[k] = v
            
    return new_dict

def objective(param):
    "Transform params, train model and return loss"


    with Capturing() as output:
            
        start = time.time()
        config = pytext_config_from_json(param)

    
        _, best_metric = train_model(
            config=config,
            device_id=4
        )

    


    stop = time.time()

    return {
        "loss": - best_metric.bootstraped_metrics[1].loc[lambda x:x.label == "Overall_metrics", "f1"].quantile(0.4),
        'status': STATUS_OK,
        "all_results": best_metric,
        "train_loss": 0,
        "best_step":0,
        "total_time":(stop-start)/3600
    }



class SpaceOptimize(object):
    def __init__(self, root_path, space = None, suggest_func = tpe.suggest):
        
        self.root_path = root_path
        self.space_path = os.path.join(root_path, "space.pickle")
        self.trials_path = os.path.join(root_path, "trials.pickle")
        self.suggest_func = suggest_func
        self.best_losses = []
        if not os.path.exists(self.root_path):
            os.makedirs(self.root_path)

        #space
        if os.path.exists(self.space_path):
            assert space is not None
            print("loading space")
            with open(self.space_path, 'rb') as h:
                self.space = pickle.load(h)
                assert self.space is not None

        else:
            print("Saving space")
            with open(self.space_path, 'wb') as h:
                pickle.dump(space, h, protocol=pickle.HIGHEST_PROTOCOL)
                self.space = space
        #trials  
        if os.path.exists(self.trials_path):
            print("loading trials")
            with open(self.trials_path, 'rb') as h:
                self.trials = pickle.load(h)
        else:
            #initiatlize trials
            self.trials = Trials()
            
    def reset(self):
        if os.path.exists(self.space_path):
            print('removing', self.space_path)
            os.remove(self.space_path)
        if os.path.exists(self.trials_path):
            print('removing', self.trials_path)
            os.remove(self.trials_path)

    def optimize(self, N_iter):
        #save every 10 runs
        for i in range(3, N_iter, 3):
            best = fmin(objective, self.space, algo=self.suggest_func, max_evals=i, trials=self.trials)
            best_loss =  min([t['result']['loss'] for t in self.trials.trials if t["result"]['status']=='ok'])
            self.best_losses.append(best_loss)
            with open(self.trials_path, 'wb') as h:
                pickle.dump(self.trials, h, protocol=pickle.HIGHEST_PROTOCOL)
                
            if i > 20:
                self.plot_trial()
                
    def get_best_param(self, new_param={}):
        
        best_param = space_eval(self.space, self.trials.argmin)
        
        best_param = modify_param(best_param, new_param)

        
        return best_param
                
    def plot_trial(self, result_type = 'loss', ylim = None):
        "Plot loss as funciton of parameters of a hyperopt param search"
        display.clear_output(wait=True)

        trial_dict = self.trials.trials
        parameters = set([t for sub in [list(t['misc']['idxs'].keys()) for t in trial_dict] for t in sub])

        f, axes = plt.subplots(nrows=int(np.ceil((len(parameters)+1)/3)), ncols=3, figsize=(15,2*(len(parameters)//3)))
        cmap = plt.cm.jet
        plt.suptitle(result_type)

        for i, val in enumerate(parameters):

            x = [t['misc']['vals'][val][0] for t in trial_dict if (t['result']['status'] == 'ok')&(not not t['misc']['vals'][val])]
            y = [t['result'][result_type] for t in trial_dict if (t['result']['status'] == 'ok')&(not not t['misc']['vals'][val])]
            x = np.array(x)
            y = np.array(y)
            axes[i//3,i%3].scatter(x, y, s=20, linewidth=0.01, alpha=0.5, c='red')
            axes[i//3,i%3].set_title(' '.join([x.capitalize() for x in val.split('_')]))

            axes[i//3,i%3].set_xlim(x.min() - x.std(), x.max() + x.std())

            if ylim is not None:
                axes[i//3,i%3].set_ylim(ylim[0], ylim[1])
                
        i+=1
        axes[i//3,i%3].plot(self.best_losses, alpha=0.5, c='red')
        axes[i//3,i%3].set_title('Best Losses')
        del i
        
        plt.subplots_adjust(hspace=0.5, top=0.9)
        display.display(plt.gcf())
        plt.close('all')
                


            


        



