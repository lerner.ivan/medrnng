#!/usr/bin/env python3
# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
import copy
from typing import Any, Dict, List, Iterable, Optional
from pytext.common.constants import DatasetFieldName, DFColumn, BatchContext
from torchtext import data as textdata
from pytext.config.field_config import FeatureConfig
from pytext.data.data_handler import DataHandler, CommonMetadata, BatchIterator
from .annotation import (
    CustomAnnotation
)
from .annotation import SHIFT,REDUCE, ROOT_LABEL, PAD_LABEL

import pickle
from pytext.data.featurizer import InputRecord, Featurizer
from pytext.fields import (
    ActionField,
    DictFeatureField,
    Field,
    FieldMeta,
    PretrainedModelEmbeddingField,
    RawField,
    TextFeatureFieldWithSpecialUnk,
)
from pytext.config import ConfigBase

from pytext.utils import cuda
import torch
import multiprocessing

from .parsing_utils import generate_valid_actions2

TREE_COLUMN = "tree"
ACTION_FEATURE_FIELD = "action_idx_feature"
ACTION_LABEL_FIELD = "action_idx_label"




class CustomMetadata:
    features: Dict[str, FieldMeta]
    target: FieldMeta
    dataset_sizes: Dict[str, int]
    valid_actions_conf:Dict={}
    keyword_dict:Dict={}
    dict_dim:int = 0



class CustomDataHandler(DataHandler):
    class Config(DataHandler.Config):
        columns_to_read: List[str] = [
            DFColumn.DOC_LABEL,
            DFColumn.WORD_LABEL,
            DFColumn.UTTERANCE,
            DFColumn.DICT_FEAT,
            DFColumn.SEQLOGICAL,
        ]
        train_batch_size: int = 1
        eval_batch_size: int = 1
        test_batch_size: int = 1
            
            
        class DictConfig(ConfigBase):
            use_dict: bool = False
            dict_dim: int = 0
            dict_path: str = ""
            dict_key:List[str] = []
            
        dict_config:DictConfig=DictConfig()
        valid_path:str = ""

    FULL_FEATURES = [
        DatasetFieldName.TEXT_FIELD,
        DatasetFieldName.DICT_FIELD,
        ACTION_FEATURE_FIELD,
        DatasetFieldName.PRETRAINED_MODEL_EMBEDDING,
    ]
    
    config: Config = Config()

    @classmethod
    def from_config(
        cls, config: Config, feature_config: FeatureConfig, *args, **kwargs
        
    ):
        
        
        
        word_feat_config = feature_config.word_feat
        features: Dict[str, Field] = {
            DatasetFieldName.TEXT_FIELD: TextFeatureFieldWithSpecialUnk(
                pretrained_embeddings_path=word_feat_config.pretrained_embeddings_path,
                embed_dim=word_feat_config.embed_dim,
                embedding_init_strategy=word_feat_config.embedding_init_strategy,
                vocab_file=word_feat_config.vocab_file,
                vocab_size=word_feat_config.vocab_size,
                vocab_from_train_data=word_feat_config.vocab_from_train_data,
                vocab_from_all_data=word_feat_config.vocab_from_all_data,
                min_freq=word_feat_config.min_freq,
                pad_token=None,
            )
        }
        if feature_config.dict_feat and feature_config.dict_feat.embed_dim > 0:
            features[DatasetFieldName.DICT_FIELD] = DictFeatureField()

        # Adding action_field to list of features so that it can be passed to
        # RNNGParser's forward method during training time.
        action_field = ActionField()  # Use the same field for label too.
        features[ACTION_FEATURE_FIELD] = action_field

        if feature_config.pretrained_model_embedding:
            features[
                DatasetFieldName.PRETRAINED_MODEL_EMBEDDING
            ] = PretrainedModelEmbeddingField(
                embed_dim=feature_config.pretrained_model_embedding.embed_dim
            )

        extra_fields: Dict[str, Field] = {
            DatasetFieldName.TOKENS: RawField(),
            DatasetFieldName.UTTERANCE_FIELD: RawField(),
        }

        return cls(
            raw_columns=config.columns_to_read,
            features=features,
            labels={ACTION_LABEL_FIELD: action_field},
            extra_fields=extra_fields,
            train_path=config.train_path,
            eval_path=config.eval_path,
            test_path=config.test_path,
            train_batch_size=config.train_batch_size,
            eval_batch_size=config.eval_batch_size,
            test_batch_size=config.test_batch_size,
            shuffle=config.shuffle,
            sort_within_batch=config.sort_within_batch,
            beam_size = config.beam_size,
            top_k = config.top_k,
            valid_path= config.valid_path,
            dict_config = {"use_dict":config.dict_config.use_dict, "dict_key":config.dict_config.dict_key, "dict_path":config.dict_config.dict_path, "dict_dim":config.dict_config.dict_dim},
            **kwargs,
        )

    
    def __init__(
        self,
        raw_columns: List[str],
        labels: Dict[str, Field],
        features: Dict[str, Field],
        featurizer: Featurizer,
        extra_fields: Dict[str, Field] = None,
        text_feature_name: str = DatasetFieldName.TEXT_FIELD,
        shuffle: bool = True,
        sort_within_batch: bool = True,
        train_path: str = "train.tsv",
        eval_path: str = "eval.tsv",
        test_path: str = "test.tsv",
        train_batch_size: int = 128,
        eval_batch_size: int = 128,
        test_batch_size: int = 128,
        max_seq_len: int = -1,
        pass_index: bool = True,
        beam_size: int = 1,
        top_k: int = 1,
        valid_path:str="",
        dict_config:dict = {},

        **kwargs,
    ) -> None:
        self.raw_columns: List[str] = raw_columns or []
        self.labels: Dict[str, Field] = labels or {}
        self.features: Dict[str, Field] = features or {}
        self.featurizer = featurizer
        self.extra_fields: Dict[str, Field] = extra_fields or {}
        if pass_index:
            self.extra_fields[BatchContext.INDEX] = RawField()
        self.text_feature_name: str = text_feature_name

        self.metadata_cls: Type = CustomMetadata
        self.metadata: CustomMetadata = CustomMetadata()
            
        if valid_path != "":
            print("loading valid config from:", valid_path)
            with open(valid_path, "rb") as h:
                valid_config = pickle.load(h)
            self.metadata.valid_actions_conf =  valid_config
        else:
            print("generating valid config from:", train_path)
            self.metadata.valid_actions_conf =  generate_valid_actions2(train_path)

                
        if dict_config["use_dict"]:
            print("use dict features with:", dict_config["dict_key"])
            ##init keyword proc
            with open(dict_config["dict_path"], "rb") as h:
                keyword_dict = pickle.load(h)
            
            keyword_dict = {k:v for k,v in keyword_dict.items() if k in dict_config["dict_key"]}
            
            self.metadata.keyword_dict = {"<$>" + k:v for k,v in keyword_dict.items()}
            self.metadata.dict_dim = dict_config["dict_dim"]
        else:
            self.metadata.keyword_dict = None

            
        self._data_cache: MutableMapping[str, Any] = {}
        self.shuffle = shuffle
        self.sort_within_batch = sort_within_batch
        self.num_workers = multiprocessing.cpu_count()
        self.max_seq_len = max_seq_len

        self.train_path = train_path
        self.eval_path = eval_path
        self.test_path = test_path
        self.train_batch_size = train_batch_size
        self.eval_batch_size = eval_batch_size
        self.test_batch_size = test_batch_size
        
        self.beam_size = beam_size
        self.top_k = top_k
        
    def _gen_extra_metadata(self):
        self.metadata.actions_vocab = self.features[ACTION_FEATURE_FIELD].vocab
        actions_vocab_dict: Dict = self.features[ACTION_FEATURE_FIELD].vocab.stoi

        # SHIFT and REDUCE indices.
        self.metadata.shift_idx: int = actions_vocab_dict[SHIFT]
        self.metadata.reduce_idx: int = actions_vocab_dict[REDUCE]
        self.metadata.root_idx: int = actions_vocab_dict[ROOT_LABEL]


        self.metadata.valid_NT_idxs: List[int] = [
            actions_vocab_dict[nt] for nt in actions_vocab_dict.keys() if nt in self.metadata.valid_actions_conf.keys()
        ]



    def _train_input_from_batch(self, batch):
        # text_input[0] is contains numericalized tokens.
        text_input = getattr(batch, DatasetFieldName.TEXT_FIELD)
        m_inputs = [text_input[0], text_input[1]]
        for name in self.FULL_FEATURES:
            if name == DatasetFieldName.TEXT_FIELD:
                continue
            input = getattr(batch, name, None)
            if name == ACTION_FEATURE_FIELD:
                input = input.tolist()  # Action needn't be passed as Tensor obj.
            m_inputs.append(input)
        # beam size and topk
        m_inputs.extend([self.beam_size, self.top_k])
        return m_inputs

    def _test_input_from_batch(self, batch):
        text_input = getattr(batch, DatasetFieldName.TEXT_FIELD)
        return [
            text_input[0],
            text_input[1],
            getattr(batch, DatasetFieldName.DICT_FIELD, None),
            None,
            getattr(batch, DatasetFieldName.PRETRAINED_MODEL_EMBEDDING, None),
            self.beam_size, 
            self.top_k
        ]

    def preprocess_row(self, row_data: Dict[str, Any]) -> Dict[str, Any]:
        utterance = row_data.get(DFColumn.UTTERANCE, "")

        
        features = self.featurizer.featurize(
            InputRecord(
                raw_text=utterance,
                raw_gazetteer_feats=row_data.get(DFColumn.DICT_FEAT, ""),
            )
        )
        actions = ""
        # training time
        if DFColumn.SEQLOGICAL in row_data:
            annotation = CustomAnnotation(row_data[DFColumn.SEQLOGICAL], utterance, valid_actions_conf= self.metadata.valid_actions_conf)
            actions = annotation.tree.to_actions()

            # Seqlogical format is required for building the tree representation of
            # compositional utterances and, it depends on tokenization.
            # Here during preprocessing, if the tokens produced from Featurizer
            # and those from the seqlogical format are not consistent, then it leads
            # to inconsistent non terminals and actions which in turn leads to
            # the model's forward method throwing an exception.
            # This should NOT happen but the check below is to make sure the
            # model training doesn't fail just in case there's inconsistency.
            tokens_from_seqlogical = annotation.tree.list_tokens()
            try:
                assert len(features.tokens) == len(tokens_from_seqlogical)
                for t1, t2 in zip(features.tokens, tokens_from_seqlogical):
                    assert t1.lower() == t2.lower()
            except AssertionError:
                print(
                    "\nTokens from Featurizer and Seqlogical format are not same "
                    + f'for the utterance "{utterance}"'
                )
                print(
                    f"{len(features.tokens)} tokens from Featurizer: {features.tokens}"
                )
                print(
                    f"{len(tokens_from_seqlogical)} tokens from Seqlogical format: "
                    + f"{tokens_from_seqlogical}"
                )
                return {}

        pretrained_model_embedding = 0
        if (
            features.pretrained_token_embedding
            and len(features.pretrained_token_embedding) > 0
        ):
            pretrained_model_embedding = features.pretrained_token_embedding

            
        return {
            DatasetFieldName.TEXT_FIELD: features.tokens,
            DatasetFieldName.DICT_FIELD: (
                features.gazetteer_feats,
                features.gazetteer_feat_weights,
                features.gazetteer_feat_lengths,
            ),
            ACTION_FEATURE_FIELD: actions,
            ACTION_LABEL_FIELD: copy.deepcopy(actions),
            DatasetFieldName.TOKENS: features.tokens,
            DatasetFieldName.UTTERANCE_FIELD: utterance,
            DatasetFieldName.PRETRAINED_MODEL_EMBEDDING: pretrained_model_embedding,
        }


    def get_predict_iter(
        self, data: Iterable[Dict[str, Any]], batch_size: Optional[int] = None, include_target: Optional[bool] = False
    ):
        ds = self.gen_dataset(data, include_label_fields=include_target)
        num_batches = (
            1 if batch_size is None else math.ceil(len(ds) / float(batch_size))
        )
        it = BatchIterator(
            textdata.Iterator(
                ds,
                batch_size=len(ds) if batch_size is None else batch_size,
                device="cuda:{}".format(torch.cuda.current_device())
                if cuda.CUDA_ENABLED
                else "cpu",
                sort=True,
                repeat=False,
                train=False,
                sort_key=self.sort_key,
                sort_within_batch=self.sort_within_batch,
                shuffle=self.shuffle,
            ),
            self._postprocess_batch,
            include_target=include_target,
            is_train=False,
            num_batches=num_batches,
        )
        if batch_size is not None:
            return it
        else:
            for input, target, context in it:
                # only return the first batch since there is only one
                return input, target, context
            
