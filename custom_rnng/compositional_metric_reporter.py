#!/usr/bin/env python3
# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved

from typing import List, Set, Union, Dict
from pytext.common.constants import BatchContext, DatasetFieldName, Stage

from .compositional_data_handler import CustomMetadata
from .annotation import (
    Token,
    Tree,
    GeneralSlot,
    TreeBuilder,
)
from .annotation import SHIFT,REDUCE, ROOT_LABEL, PAD_LABEL

from .intent_slot_metrics import (
    FramePredictionPair,
    Node,
    Span,
    compute_all_metrics,
)

from pytext.metric_reporters.channel import Channel, ConsoleChannel, FileChannel
from pytext.metric_reporters.metric_reporter import MetricReporter


PRED_TARGET_TREES = "pred_target_trees"
ALL_PRED_FRAMES = "all_pred_frames"


class CompositionalFileChannel(FileChannel):
    def get_title(self):
        return ("doc_index", "text", "predicted_annotation", "actual_annotation")

    def gen_content(self, metrics, loss, preds, targets, scores, context):
        for index, utterance, (pred_tree, target_tree) in zip(
            context[BatchContext.INDEX],
            context[DatasetFieldName.UTTERANCE_FIELD],
            context[PRED_TARGET_TREES],
        ):
            yield (index, utterance, pred_tree.flat_str(), target_tree.flat_str())


class CustomMetricReporter(MetricReporter):
    def __init__(self, actions_vocab, valid_actions_conf: Dict,  channels: List[Channel]) -> None:
        super().__init__(channels)
        self.actions_vocab = actions_vocab.itos
        self.stoi = actions_vocab.stoi
        self.n_bootstrap = 1000
        self.valid_actions_conf = valid_actions_conf

    @classmethod
    def from_config(cls, config, metadata: CustomMetadata):
        actions_vocab = metadata.actions_vocab
        
        valid_actions_conf = metadata.valid_actions_conf
        
        return cls(
            actions_vocab,
            valid_actions_conf,
            [
                ConsoleChannel(),
                CompositionalFileChannel((Stage.TEST,), config.output_path),
            ],
        )

    def gen_extra_context(self):
        # check if all_preds contains top K results or only 1 result
        pred_target_trees = []
        all_pred_trees: List[List[Tree]] = []

        for top_k_action_preds, action_targets, token_str_list in zip(
            self.all_preds, self.all_targets, self.all_context[DatasetFieldName.TOKENS]
        ):
            topk_pred_trees = []
            for k, action_preds in enumerate(top_k_action_preds):
                pred_tree = CustomMetricReporter.tree_from_tokens_and_indx_actions(
                    token_str_list, self.actions_vocab, action_preds, self.valid_actions_conf
                )
                topk_pred_trees.append(
                    CustomMetricReporter.tree_to_metric_node(pred_tree)
                )
                if k == 0:
                    target_tree = CustomMetricReporter.tree_from_tokens_and_indx_actions(
                        token_str_list, self.actions_vocab, action_targets, self.valid_actions_conf
                    )
                    pred_target_trees.append((pred_tree, target_tree))
            all_pred_trees.append(topk_pred_trees)
        self.all_context[PRED_TARGET_TREES] = pred_target_trees
        self.all_context[ALL_PRED_FRAMES] = all_pred_trees

    # CREATE NODES
    def calculate_metric(self):
        return compute_all_metrics(
            self.create_frame_prediction_pairs(),
            overall_metrics=True,
            all_predicted_frames=self.all_context[ALL_PRED_FRAMES],
            token_wise = True,
            token_pred = self.all_preds,
            token_target = self.all_targets,
            stoi = self.stoi,
            n_bootstrap = self.n_bootstrap
        )

    def create_frame_prediction_pairs(self):
        return [
            FramePredictionPair(
                CustomMetricReporter.tree_to_metric_node(pred_tree),
                CustomMetricReporter.tree_to_metric_node(target_tree),
            )
            for pred_tree, target_tree in self.all_context[PRED_TARGET_TREES]
        ]

    def get_model_select_metric(self, metrics):
        """return fourtiest percentile, trades-off between optimizing the expected f1 and its variability"""
        return metrics.bootstraped_metrics[1].loc[lambda x:x.label == "Overall_metrics", "f1"].quantile(0.4)


    @staticmethod
    def tree_from_tokens_and_indx_actions(
        token_str_list: List[str], actions_vocab: List[str], actions_indices: List[int], valid_actions_conf = Dict
    ):
        builder = TreeBuilder()
        i = 0

        for action_idx in actions_indices:
            action = actions_vocab[action_idx]
            if action == REDUCE:
                builder.update_tree(action, None)
            elif action == SHIFT:
                builder.update_tree(action, token_str_list[i])
                i += 1
            else:
                builder.update_tree(action, action)
        tree = builder.finalize_tree(valid_actions_conf)
        return tree

    @staticmethod
    def tree_to_metric_node(tree: Tree) -> Node:
        """
        Creates a Node from tree assuming the utterance is a concatenation of the
        tokens by whitespaces. The function does not necessarily reproduce the original
        utterance as extra whitespaces can be introduced.
        """
        return CustomMetricReporter.node_to_metrics_node(tree.root) #.children[0]

    @staticmethod
    def node_to_metrics_node(node: GeneralSlot, start: int = 0) -> Node:
        """
        The input start is the absolute start position in utterance
        """
        res_children: Set[Node] = set()
        idx = start
        for child in node.children:
            if type(child) == Token:
                idx += len(child.label) + 1
            elif child.label != ROOT_LABEL:
                res_child = CustomMetricReporter.node_to_metrics_node(child, idx)
                res_children.add(res_child)
                idx = res_child.span.end + 1
            else:
                raise ValueError("Child must be Token, Intent or Slot!")
        node = Node(label=node.label, span=Span(start, idx - 1), children=res_children)
        return node