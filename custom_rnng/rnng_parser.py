from typing import List, Optional, Tuple, Dict, Union
from pytext.common.constants import Stage


from pytext.models.representations.bilstm import BiLSTM
from pytext.config.component import Component, ComponentType


from pytext.config import ConfigBase
from typing import List, Optional, Tuple
import numpy as np
import pytext.utils.cuda as cuda_utils
import torch
import torch.nn as nn
import torch.nn.functional as F
from pytext.models.embeddings import EmbeddingList
from pytext.config.component import Component
from pytext.models.output_layers.word_tagging_output_layer import OutputLayerBase, SEQ_LENS

from flashtext import KeywordProcessor
import unidecode
import pickle

#init model
from enum import Enum
from .data_structures import *
from pytext.models import Model

from .compositional_data_handler import CustomMetadata
import allennlp.commands.elmo

class RNNGner(Model, Component):
    """
    The Recurrent Neural Network Grammar (RNNG) parser from
    Dyer et al.: https://arxiv.org/abs/1602.07776 and
    Gupta et al.: https://arxiv.org/abs/1810.07942d.
    RNNG is a neural constituency parsing algorithm that
    explicitly models compositional structure of a sentence.
    It is able to learn about hierarchical relationship among the words and
    phrases in a given sentence thereby learning the underlying tree structure.
    The paper proposes generative as well as discriminative approaches.
    In PyText we have implemented the discriminative approach for modeling
    intent slot models.
    It is a top-down shift-reduce parser than can output
    trees with non-terminals (intent and slot labels) and terminals (tokens)
    """

    __COMPONENT_TYPE__ = ComponentType.MODEL

    class Config(ConfigBase):
        class CompositionalType(Enum):
            """Whether to use summation of the vectors or a BiLSTM based composition to
             generate embedding for a subtree"""

            BLSTM = "blstm"
            SUM = "sum"
            

            

            
        class AblationParams(ConfigBase):
            """Ablation parameters.

            Attributes:
                use_buffer (bool): whether to use the buffer LSTM
                use_stack (bool): whether to use the stack LSTM
                use_action (bool): whether to use the action LSTM
                use_last_open_NT_feature (bool): whether to use the last open
                    non-terminal as a 1-hot feature when computing representation
                    for the action classifier
            """
            class LossType(Enum):
                CE = "cross_entropy"
                LS = "label_smoothing"


                
                
            loss_type: LossType = LossType.CE
            smoothing: float = 0.15

            use_buffer: bool = True
            use_stack: bool = True
            use_action: bool = True
            use_last_open_NT_feature: bool = False
                
            use_constraints: bool = True
            use_oracle: bool = True
                
            use_elmo: bool = False
            embedding_dropout: float = 0.5



                    

        class RNNGConstraints(ConfigBase):
            """Constraints when computing valid actions.

            Attributes:
                intent_slot_nesting (bool): for the intent slot models, the top level
                    non-terminal has to be an intent, an intent can only have slot
                    non-terminals as children and vice-versa.

                ignore_loss_for_unsupported (bool): if the data has "unsupported" label,
                    that is if the label has a substring "unsupported" in it, do not
                    compute loss
                no_slots_inside_unsupported (bool): if the data has "unsupported" label,
                    that is if the label has a substring "unsupported" in it, do not
                    predict slots inside this label.
            """

            intent_slot_nesting: bool = True
            ignore_loss_for_unsupported: bool = False
            no_slots_inside_unsupported: bool = True

        # version 0 - initial implementation
        # version 1 - beam search
        # version 2 - use zero init state rather than random
        # version 3 - add beam search input params
        # version 4 - ELMo4med extract

        version: int = 4
        lstm: BiLSTM.Config = BiLSTM.Config()
        ablation: AblationParams = AblationParams()
        constraints: RNNGConstraints = RNNGConstraints()
        max_open_NT: int = 10
        dropout: float = 0.1
        beam_size: int = 1
        top_k: int = 1
        compositional_type: CompositionalType = CompositionalType.BLSTM

    @classmethod
    def from_config(cls, model_config, feature_config, metadata: CustomMetadata, device):
        #init compositional type
        if model_config.compositional_type == RNNGner.Config.CompositionalType.SUM:
            p_compositional = CompositionalSummationNN(
                lstm_dim=model_config.lstm.lstm_dim
            )
        elif (
            model_config.compositional_type == RNNGner.Config.CompositionalType.BLSTM
        ):
            p_compositional = CompositionalNN(lstm_dim=model_config.lstm.lstm_dim)
        else:
            raise ValueError(
                "Cannot understand compositional flag {}".format(
                    model_config.compositional_type
                )
            )
        
        #init embedding
        
        if model_config.ablation.use_elmo:
            print('loading elmo...')

            embedding = allennlp.commands.elmo.ElmoEmbedder(
                options_file=feature_config.word_feat.pretrained_embeddings_path + "options.json", weight_file=feature_config.word_feat.pretrained_embeddings_path + "weights.hdf5"
            )
            
    
    
            embedding.embedding_dim = embedding.elmo_bilm.get_output_dim() * 3
        else:
            embedding = Model.create_embedding(feature_config, metadata)
            # self.embedding.config: FeatureConfig object cannot be pickled but,
            # we require the model to be pickled for passing from one worker process
            # for Hogwild training. Hence, setting the config to None
            embedding.config = None
            



        if model_config.ablation.loss_type == RNNGner.Config.ablation.loss_type.CE:
            loss_func = nn.CrossEntropyLoss()
            
        elif model_config.ablation.loss_type == RNNGner.Config.ablation.loss_type.LS:
            loss_func = LabelSmoothingLoss(classes =  len(metadata.actions_vocab), smoothing = model_config.ablation.smoothing)


        else:
            assert False, "loss not defined"
        
        print("loss:", model_config.ablation.loss_type )
        
        
        if metadata.keyword_dict:
            keyword_processor = KeywordProcessor()
            keyword_processor._white_space_chars = {' '}
            keyword_processor.add_keywords_from_dict(metadata.keyword_dict)
            dict_vocab = {v:i for i,v in enumerate(["<$>OUTSIDE_KEY"]  + list(metadata.keyword_dict.keys()))}
#             assert any([len(t) <= len("<$>OUTSIDE_KEY") for t in self.dict_vocab.keys()]), "too long keys"
            dict_embedding = nn.Embedding(len(dict_vocab), metadata.dict_dim)
        else:
            keyword_processor = None
            dict_embedding = None
            dict_vocab = None


    
        return cls(
            ablation=model_config.ablation,
            lstm_num_layers=model_config.lstm.num_layers,
            lstm_dim=model_config.lstm.lstm_dim,
            max_open_NT=model_config.max_open_NT,
            dropout=model_config.dropout,
            metadata=metadata,
            embedding=embedding,
            p_compositional=p_compositional,
            device=device,
            loss_func = loss_func,
            keyword_processor = keyword_processor,
            dict_embedding = dict_embedding,
            dict_vocab = dict_vocab
        )

    def __init__(
        self,
        ablation: Config.AblationParams,
        metadata: CustomMetadata,
        lstm_num_layers: int,
        lstm_dim: int,
        max_open_NT: int,
        dropout: float,
        embedding: Union[torch.nn.Module, allennlp.commands.elmo.ElmoEmbedder],
        p_compositional: CompositionFunction,
        device: object,
        loss_func:nn.Module,
        keyword_processor:KeywordProcessor ,
        dict_embedding: nn.Embedding,
        dict_vocab: dict
    ) -> None:
        """
        Initialize the model

        Args:
        ablation : AblationParams
            Features/RNNs to use
        lstm_num_layers : int
            number of layers in the LSTMs
        lstm_dim : int
            size of LSTM
        max_open_NT : int
            number of maximum open non-terminals allowed on the stack.
            After that, the only valid actions are SHIFT and REDUCE
        dropout : float
            dropout parameter
        beam_size : int
            beam size for beam search; run only during inference
        top_k : int
            top k results from beam search
        actions_vocab : Vocab (right now torchtext.vocab.Vocab)
            dictionary of actions
        embedding : EmbeddingList
            embeddings for the tokens
        p_compositional : CompositionFunction
            Composition function to use to get embedding of a sub-tree


        Returns:
        None


        """

        nn.Module.__init__(self)
        

        self.embedding = embedding
        self.device = device
        self.loss_func = loss_func
        self.keyword_processor = keyword_processor
        self.dict_embedding = dict_embedding
        self.dict_vocab = dict_vocab
        
        self.p_compositional = p_compositional
        self.ablation_use_last_open_NT_feature = ablation.use_last_open_NT_feature
        self.ablation_use_buffer = ablation.use_buffer
        self.ablation_use_stack = ablation.use_stack
        self.ablation_use_action = ablation.use_action
        self.ablation_use_constraints = ablation.use_constraints
        self.ablation_use_oracle = ablation.use_oracle

        self.ablation_use_elmo = ablation.use_elmo

        #old

        self.lstm_num_layers = lstm_num_layers
        self.lstm_dim = lstm_dim
        self.max_open_NT = max_open_NT
        self.actions_vocab = metadata.actions_vocab
        self.shift_idx = metadata.shift_idx
        self.reduce_idx = metadata.reduce_idx
        self.root_idx = metadata.root_idx
        self.valid_NT_idxs = metadata.valid_NT_idxs
        self.valid_actions_conf =  metadata.valid_actions_conf
        
        if self.ablation_use_constraints:
            print("use constraints:", {k:v.keys() for k,v in self.valid_actions_conf.items()})
        print("use actions vocab:", self.actions_vocab.stoi)
        
        num_actions = len(metadata.actions_vocab)
        lstm_count = ablation.use_buffer + ablation.use_stack + ablation.use_action
        if lstm_count == 0:
            raise ValueError("Need at least one of the LSTMs to be true")

            


        self.action_linear = nn.Sequential(
            nn.Linear(
                lstm_count * lstm_dim + num_actions * ablation.use_last_open_NT_feature,
                lstm_dim,
            ),
            nn.ReLU(),
            nn.Linear(lstm_dim, num_actions),
        )
        self.dropout_layer = nn.Dropout(p=dropout)
        
            
        self.buff_rnn = nn.LSTM(
            embedding.embedding_dim + ( dict_embedding.embedding_dim if dict_embedding else 0),
            lstm_dim,
            num_layers=lstm_num_layers,
            dropout=dropout
        )
        self.stack_rnn = nn.LSTM(
            lstm_dim, lstm_dim, num_layers=lstm_num_layers, dropout=dropout
        )
        self.action_rnn = nn.LSTM(
            lstm_dim, lstm_dim, num_layers=lstm_num_layers, dropout=dropout
        )

        self.actions_lookup = nn.Embedding(num_actions, lstm_dim)
        

        #to cuda
#         self.to(self.device)
            
    def forward(
        self,
        tokens: torch.Tensor,
        seq_lens: torch.Tensor,
        dict_feat: Optional[Tuple[torch.Tensor, ...]] = None,
        actions: Optional[List[List[int]]] = None,
        contextual_token_embeddings: Optional[torch.Tensor] = None,
        beam_size=1,
        top_k=1,
    ) -> List[Tuple[torch.Tensor, torch.Tensor]]:
        """RNNG forward function.

        Args:
            tokens (torch.Tensor): list of tokens
            seq_lens (torch.Tensor): list of sequence lengths
            dict_feat (Optional[Tuple[torch.Tensor, ...]]): dictionary or gazetteer
                features for each token
            actions (Optional[List[List[int]]]): Used only during training.
                Oracle actions for the instances.

        Returns:
            list of top k tuple of predicted actions tensor and corresponding scores tensor.
            Tensor shape:
            (batch_size, action_length)
            (batch_size, action_length, number_of_actions)
        """

        if self.stage != Stage.TEST:
            beam_size = 1
            top_k = 1


        if self.training:
            assert actions is not None, "actions must be provided for training"
            actions_idx_rev = list(reversed(actions[0]))
            if self.ablation_use_oracle:
                use_oracle = True
            else:
                use_oracle = torch.rand(1).item() < 0.9
                
        else:
            torch.manual_seed(0)

        beam_size = max(1, beam_size)

        # Reverse the order of input tokens.
        tokens_list_rev = torch.flip(tokens, [len(tokens.size()) - 1])

        if self.ablation_use_elmo:

            
            token_embeddings = self.embedding.embed_batch([t[::-1] for t in self.context['tokens']]) #flip
            assert len(token_embeddings) == 1 #batch 1
            token_embeddings = torch.cat(torch.split(torch.FloatTensor(token_embeddings[0]),1 ,0), 2).to(self.device)
            
            
        else:
            # Aggregate inputs for embedding module.
            embedding_input = [tokens]



            # Embed and reverse the order of tokens.
            token_embeddings = self.embedding(*embedding_input)
            token_embeddings = torch.flip(token_embeddings, [len(tokens.size()) - 1])
            
        if self.dict_embedding:
            dict_feat = self.tensorize_dict_feat() #from context
            embedded_dict_feat = self.dict_embedding(dict_feat)
            token_embeddings = torch.cat([token_embeddings, embedded_dict_feat], 2)

                
        # Batch size is always = 1. So we squeeze the batch_size dimension.
        token_embeddings = token_embeddings.squeeze(0)
        tokens_list_rev = tokens_list_rev.squeeze(0)

        initial_state = CustomParserState(self)
        for i in range(token_embeddings.size()[0]):
            token_embedding = token_embeddings[i].unsqueeze(0)
            tok = tokens_list_rev[i]
            initial_state.buffer_stackrnn.push(token_embedding, Element(tok))
            

            

                    
        beam = [initial_state]
        while beam and any(not state.finished() for state in beam):
            # Stores plans for expansion as (score, state, action)
            plans: List[Tuple[float, CustomParserState, int]] = []
            # Expand current beam states
            for state in beam:
                # Keep terminal states
                if state.finished():
                    plans.append((state.neg_prob, state, -1))
                    continue

                #  translating Expression p_t = affine_transform({pbias, S,
                #  stack_summary, B, buffer_summary, A, action_summary});
                stack = state.stack_stackrnn
                stack_summary = stack.embedding()
                action_summary = state.action_stackrnn.embedding()
                buffer_summary = state.buffer_stackrnn.embedding()
                

                if self.dropout_layer.p > 0:
                    stack_summary = self.dropout_layer(stack_summary)
                    action_summary = self.dropout_layer(action_summary)
                    buffer_summary = self.dropout_layer(buffer_summary)

                # feature for index of last open non-terminal
                last_open_NT_feature = torch.zeros(len(self.actions_vocab))
                open_NT_exists = state.num_open_NT > 0

                if (
                    len(stack) > 0
                    and open_NT_exists
                    and self.ablation_use_last_open_NT_feature
                ):
                    last_open_NT = None
                    try:
                        open_NT = state.is_open_NT[::-1].index(True)
                        last_open_NT = stack.element_from_top(open_NT)
                    except ValueError:
                        pass
                    if last_open_NT:
                        last_open_NT_feature[last_open_NT.node] = 1.0
                last_open_NT_feature = last_open_NT_feature.unsqueeze(0)

                summaries = []
                if self.ablation_use_buffer:
                    summaries.append(buffer_summary)
                if self.ablation_use_stack:
                    summaries.append(stack_summary)
                if self.ablation_use_action:
                    summaries.append(action_summary)
                if self.ablation_use_last_open_NT_feature:
                    summaries.append(last_open_NT_feature)

                state.action_p = self.action_linear(torch.cat(summaries, dim=1))

                log_probs = F.log_softmax(state.action_p, dim=1)[0]

                for action in self.valid_actions(state):
                    plans.append(
                        (state.neg_prob - log_probs[action].item(), state, action)
                    )

            beam = []
            # Take actions to regenerate the beam
            for neg_prob, state, predicted_action_idx in sorted(plans)[:beam_size]:
                # Skip terminal states
                if state.finished():
                    beam.append(state)
                    continue

                # Only branch out states when needed
                if beam_size > 1:
                    #copy before push
                    state = state.copy()

                state.predicted_actions_idx.append(predicted_action_idx)
                
                #if eval push predicted action
                target_action_idx = predicted_action_idx
                if self.training and use_oracle:
                    assert (
                        len(actions_idx_rev) > 0
                    ), "Actions and tokens may not be in sync."
                    #if training push next oracle action
                    target_action_idx = actions_idx_rev[-1]
                    actions_idx_rev = actions_idx_rev[:-1]

                #compute loss with pred action proba
                state.action_scores.append(state.action_p)
                self.push_action(state, target_action_idx)
                state.neg_prob = neg_prob
                beam.append(state)
            # End for
        # End while
        assert len(beam) > 0, "How come beam is empty?"
        assert len(state.stack_stackrnn) == 1, "How come stack len is " + str(
            len(state.stack_stackrnn)
        )
        assert len(state.buffer_stackrnn) == 0, "How come buffer len is " + str(
            len(state.buffer_stackrnn)
        )


        
        logits = [
            (
                cuda_utils.LongTensor(state.predicted_actions_idx).unsqueeze(0),
                torch.cat(state.action_scores).unsqueeze(0),
            )
            for state in sorted(beam)[:top_k]
        ]
        
        
        return logits


    def tensorize_dict_feat(self):
        utterrance = unidecode.unidecode(self.context['utterance'][0])
        dict_vocab = self.dict_vocab
        #match dict
        span_match = self.keyword_processor.extract_keywords(utterrance, span_info = True)
        
        #get spans
        spans = np.cumsum([len(t) + 1 for t in utterrance.split(' ')])
        spans = np.insert(spans, 0, 0)
        spans = np.vstack((spans[:-1], spans[1:] -1) ).T

        #insert match
        starts = np.searchsorted(spans[:,0], [t[1] for t in span_match])
        stops = np.searchsorted(spans[:,1], [t[2] for t in span_match], side ='right')

        dic_feat = np.repeat("<$>OUTSIDE_KEY", spans.shape[0])
        for tag, start, stop in zip([t[0] for t in span_match], starts, stops):  
            dic_feat[start:stop] = tag

        #reformat to torch
        dic_feat = np.vectorize(dict_vocab.get)(dic_feat)
        dic_feat = torch.LongTensor(dic_feat).to(self.device)
        
        assert len(self.context['tokens'][0]) == len(dic_feat), "Wrong split?"
        
        return dic_feat.unsqueeze(0)
        
    def valid_actions(self, state: CustomParserState) -> List[int]:
        """Used for restricting the set of possible action predictions

        Args:
            state (ParserState): The state of the stack, buffer and action

        Returns:
            List[int] : indices of the valid actions

        """
        valid_actions: List[int] = []
        is_open_NT = state.is_open_NT
        num_open_NT = state.num_open_NT
        stack = state.stack_stackrnn
        buffer = state.buffer_stackrnn
        

        #1. start by opening root
        if not is_open_NT:
            valid_actions.append(self.root_idx)
            
        #2. open slots
        elif (len(buffer) > 0) and (num_open_NT < self.max_open_NT):
            #use constraints during eval if specified
            if (not(self.ablation_use_oracle) or not(self.training)) and self.ablation_use_constraints:
                last_open_NT = stack.element_from_top(is_open_NT[::-1].index(True)) #index for last openNT
                valid_actions += state.valid_actions_set

                #can reduce if more than 1 open NT and last open NT not empty
                if num_open_NT > 1 and not is_open_NT[-1]:
                    pass
                else:
                    valid_actions = [t for t in valid_actions if t!=self.reduce_idx]

                    
            #open any slots without constraints
            else:

                #can reduce if more than 1 open NT and last open NT not empty
                if (num_open_NT > 1) and (not is_open_NT[-1]):
                    valid_actions.append(self.reduce_idx)
                valid_actions += self.valid_NT_idxs
                valid_actions.append(self.shift_idx)

                
        #3. reduce all if buffer empty
        elif len(buffer) == 0:
            assert len(stack) > 0
            valid_actions.append(self.reduce_idx)

        #4. consume all buffer if max depth reached
        elif num_open_NT >= self.max_open_NT:
            valid_actions.append(self.shift_idx)

        else:
            assert False, "Non valid actions config"
  
        return valid_actions
    

    
    def push_action(self, state: CustomParserState, target_action_idx: int) -> None:
        """Used for updating the state with a target next action

        Args:
            state (ParserState): The state of the stack, buffer and action
            target_action_idx (int): Index of the action to process
        """

        #update valid action
        if (not(self.ablation_use_oracle) or not(self.training)) and self.ablation_use_constraints:
            state.update_valid_actions_stack(target_action_idx)
            state.valid_actions_set = state.get_valid_from_stack()

        # Update action_stackrnn
        action_embedding = self.actions_lookup(
            cuda_utils.Variable(torch.LongTensor([target_action_idx]))
        )
        state.action_stackrnn.push(action_embedding, Element(target_action_idx))

        # Update stack_stackrnn
        if target_action_idx == self.shift_idx:
            # To SHIFT,
            # 1. Pop T from buffer
            # 2. Push T into stack
            state.is_open_NT.append(False)
            
            token_embedding, token = state.buffer_stackrnn.pop()
            state.stack_stackrnn.push(token_embedding, Element(token))

        elif target_action_idx == self.reduce_idx:
            # To REDUCE
            # 1. Pop Ts from stack until hit NT
            # 2. Pop the open NT from stack and close it
            # 3. Compute compositionalRep and push into stack
            state.num_open_NT -= 1
            popped_rep = []
            nt_tree = []

            while not state.is_open_NT[-1]:

                assert len(state.stack_stackrnn) > 0, "How come stack is empty!"
                state.is_open_NT.pop()
                top_of_stack = state.stack_stackrnn.pop()
                popped_rep.append(top_of_stack[0])
                nt_tree.append(top_of_stack[1])

            # pop the open NT and close it
            top_of_stack = state.stack_stackrnn.pop()
            popped_rep.append(top_of_stack[0])
            nt_tree.append(top_of_stack[1])

            state.is_open_NT.pop()
            state.is_open_NT.append(False)

            compostional_rep = self.p_compositional(popped_rep)
            combinedElement = Element(nt_tree)

            state.stack_stackrnn.push(compostional_rep, combinedElement)

        elif target_action_idx in self.valid_NT_idxs:
            state.is_open_NT.append(True)
            state.num_open_NT += 1
            state.stack_stackrnn.push(action_embedding, Element(target_action_idx))
        else:
            assert "not a valid action: {}".format(
                self.actions_vocab.itos[target_action_idx]
            )

    def get_param_groups_for_optimizer(self):
        """
        This is called by code that looks for an instance of pytext.models.model.Model.
        """
        return [{"params": self.parameters()}]

    def get_loss(
        self,
        logits: List[Tuple[torch.Tensor, torch.Tensor]],
        target_actions: torch.Tensor,
        context: torch.Tensor,
    ):
        """
        Shapes:
            logits[1]: action scores: (1, action_length, number_of_actions)
            target_actions: (1, action_length)
        """
        # squeeze to get rid of the batch dimension
        # logits[0] is the top1 result
        
            
        action_scores = logits[0][1].squeeze(0)
        target_actions = target_actions[0].squeeze(0)


        if not self.ablation_use_oracle:
            length_diff = target_actions.size()[0] - action_scores.size()[0]
            if length_diff > 0: #pad pred
                action_scores = F.pad(action_scores, (0, length_diff, 0, 0), "constant", self.shift_idx)

            elif length_diff < 0: #pad target
                target_actions = F.pad(target_actions, (0, -length_diff), "constant", self.shift_idx)


        action_scores_list = torch.chunk(action_scores, action_scores.size()[0])
        target_vars = torch.chunk(target_actions, target_actions.size()[0])
        
        
        
        
        losses = [
                self.loss_func(action, target).view(1)
                for action, target in zip(action_scores_list, target_vars)
            ]
        total_loss = torch.sum(torch.cat(losses)) if len(losses) > 0 else None


        return total_loss

    def get_single_pred(self, logits: Tuple[torch.Tensor, torch.Tensor], *args):
        predicted_action_idx, predicted_action_scores = logits
    
    
        predicted_scores = [
            np.exp(np.max(action_scores)).item() / np.sum(np.exp(action_scores)).item()
            for action_scores in predicted_action_scores.detach().squeeze(0).tolist()
        ]
        # remove the batch dimension since it's only 1
        predicted_action_idx = predicted_action_idx.tolist()[0]
        
            


        return predicted_action_idx, predicted_scores

    # Supports beam search by checking if top K exists return type
    def get_pred(self, logits: List[Tuple[torch.Tensor, torch.Tensor]], *args):
        """
        Return Shapes:
            preds: batch (1) * topk * action_len
            scores: batch (1) * topk * (action_len * number_of_actions)
        """
        n = len(logits)
        all_action_idx: List[List[int]] = [[]] * n
        all_scores: List[List[float]] = [[]] * n
        for i, l in enumerate(logits):
            all_action_idx[i], all_scores[i] = self.get_single_pred(l, *args)

        # add back batch dimension
        return [all_action_idx], [all_scores]




    def save_modules(self, *args, **kwargs):
        pass

    def contextualize(self, context):
        self.context = context
        
        
class LabelSmoothingLoss(nn.Module):
    def __init__(self, classes, smoothing=0.0, dim=-1):
        super(LabelSmoothingLoss, self).__init__()
        self.confidence = 1.0 - smoothing
        self.smoothing = smoothing
        self.cls = classes
        self.dim = dim

    def forward(self, pred, target):
        pred = pred.log_softmax(dim=self.dim)
        with torch.no_grad():
            # true_dist = pred.data.clone()
            true_dist = torch.zeros_like(pred)
            true_dist.fill_(self.smoothing / (self.cls - 1))
            true_dist.scatter_(1, target.data.unsqueeze(1), self.confidence)
        return torch.mean(torch.sum(-true_dist * pred, dim=self.dim))

