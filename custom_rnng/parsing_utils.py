import pandas as pd
from .annotation import CustomAnnotation
from .annotation import SHIFT,REDUCE, ROOT_LABEL, PAD_LABEL

from nltk import Tree
import numpy as np
from copy import deepcopy

############################GENEREATE VALID ACTION ##################################
def get_valid(sentence, valid_actions = {}):
    """Parse sentence and get parents/child combinations from annotated sentence"""
    stack = []
    for i, head in enumerate([t for t in sentence.split() if t[0] in ["[", "]"]]):
        if head[0] == "[":
            if head[1:] not in valid_actions.keys():
                valid_actions[head[1:]] = set([SHIFT])
            if len(stack)>0:
                valid_actions[stack[-1]].add(head[1:])
            stack.append(head[1:])
        if head[0] == "]":
            stack.pop()
    
    return valid_actions


def generate_valid_actions(path):
    """Generate config for valid actions from annotated data in the RNNG format"""
    ### Load test data
    with open(path, "r") as h:
        data = []
        for sent in h.readlines():
            data.append(sent.strip().split('\t')[1])
    
    #get va
    va = {ROOT_LABEL:set([ROOT_LABEL, SHIFT, REDUCE])}
    for sent in data:
        va = get_valid(sent, va)
    
    return va

def update_valid(valid_actions, constituant):
    key = list(constituant.keys())[0]
    for k in constituant[key].keys():
        tmp_count = constituant[key][k]
        valid_actions[key][k]['max'] = max(valid_actions[key][k]['max'], tmp_count)
        valid_actions[key][k]['min'] = min(valid_actions[key][k]['min'], tmp_count)
    return valid_actions

def get_counts_and_valid(valid_actions, sentence, z):
    counts = {k:{u:0 for u in v} for k,v in valid_actions.items()}
    stack = []
    top_stack = []
    for i, head in enumerate([t for t in sentence.split()]):
        #get top stack
        if stack:
            top_stack = stack[-1]
            pointer = list(top_stack.keys())[0]

        #open constituent
        if head[0] == "[":
            action = head[1:]

            if top_stack:
                top_stack[pointer][action] += 1

#                 if (ANY_LABEL[0] in top_stack[pointer].keys()) & (action in ANY_LABEL[1]):
#                     top_stack[pointer][ANY_LABEL[0]] += 1
#                 if (ANY_DRUG_RULE[0] in top_stack[pointer].keys()) & (action in ANY_DRUG_RULE[1]):
#                     top_stack[pointer][ANY_DRUG_RULE[0]] += 1
#                 if (ANY_ATTR_RULE[0] in top_stack[pointer].keys()) & (action in ANY_ATTR_RULE[1]):
#                     top_stack[pointer][ANY_ATTR_RULE[0]] += 1
#                 if (ANY_BLOB_RULE[0] in top_stack[pointer].keys()) & (action in ANY_BLOB_RULE[1]):
#                     top_stack[pointer][ANY_BLOB_RULE[0]] += 1

            #update stack state
            stack.append({action:counts[action]})

        #close constituent
        elif head[0] == "]":
            action = REDUCE
            constituant = stack.pop()
                    
            update_valid(valid_actions, constituant)

        #add tokens
        else:
            action = SHIFT
            top_stack[pointer][action] += 1

    assert not stack
#     assert valid_actions["DRUG_BLOB"]["ANY_DRUG"]['min'] > 0
#     assert valid_actions["ORDO_DRUG_BLOB"]["ANY_BLOB"]['min'] > 0

    return valid_actions


def generate_valid_actions2(path, correction = 2):
    """Generate config for valid actions from annotated data in the RNNG format"""
    ### Load test data
    if type(path) == str:
        with open(path, "r") as h:
            data = []
            for sent in h.readlines():
                data.append(sent.strip().split('\t')[1])
    elif type(path)==list:
        data = []
        for p in path:
            with open(p, "r") as h:
                for sent in h.readlines():
                    data.append(sent.strip().split('\t')[1])
    
    #get va
    va = {}
    for sent in data:
        va = get_valid(sent, va)

    #add rules
    va_with_rule = {}
    for k,v in va.items():
        va_with_rule[k] = v
#         if set(ANY_DRUG_RULE[1]).issubset(v):
#             va_with_rule[k].add(ANY_DRUG_RULE[0])
#         if set(ANY_ATTR_RULE[1]).issubset(v):
#             va_with_rule[k].add(ANY_ATTR_RULE[0])
#         if set(ANY_BLOB_RULE[1]).issubset(v):
#             va_with_rule[k].add(ANY_BLOB_RULE[0])
#         if set(ANY_LABEL[1]).issubset(v):
#             va_with_rule[k].add(ANY_LABEL[0])


    va_with_counts = {k:{u:{"min":1000, "max":0} for u in v} for k,v in va_with_rule.items()}

    #compute min max
    for z, sent in enumerate(data):
        va_with_counts = get_counts_and_valid(va_with_counts, sent, z)
        
    va = va_with_counts
    #apply correction
    if correction == 1:
        print("applying a x3 correction to maximum and minimum <=1")
        for k,v in va.items():
            for u,w in v.items():
                w['max'] = w['max']*3
                w['min'] = min(w['min'], 1)

#         va[ROOT_LABEL][ROOT_LABEL] = {'min': 0, 'max': 1}

    elif correction == 2:
        print("minimal constraints")

        no_constraints = {}
        for k,_ in va.items():
            no_constraints[k] = {k:{"min":0, "max":1000} for k in va.keys() if k!=ROOT_LABEL}
            no_constraints[k][SHIFT] = {"min":0, "max":1000}
            
        va = no_constraints

    return va




#############################BRAT 2 RNNG #####################################################
def multiple_conll2brat(pdf, colnames):
    """Transform a CONLL annotation to a BRAT annotation
    
    Parameters:
    conll (pdDataframe): a pandas dafaframe with multiple columsn
    conlames (list) : list of columns to consider for brat

    Returns:
    txt: the raw text
    annotations: in brat format
    """    
    ann_acc = []
    for col in colnames:
        txt, tmp_ann = conll2brat(pdf.loc[:,["tok", col]].rename(columns={col: "lab"}))
        ann_acc.append(tmp_ann)
    ann_acc = pd.concat(ann_acc, axis= 0)

    return txt, ann_acc

def check_brat_file(txt, ann):
    """Check the validity of a brat file
    
    Parameters:
    text (str): the raw text
    ann (pdDataframe) : a pandas dataframe with brat annotations style

    Returns:
    Assertion error is annotations not in text
    """    
    for i, row in ann.iterrows():
        _, start, stop = row.ent.split()
        assert txt[int(start): int(stop)] == row.txt

def get_conll_chunk_id(pdf):
    """Split a conll file by chunk
    
    Parameters:
    pdf (pdDataframe): annotated tokens in conll format

    Returns:
    chunk_ids:id of each chunk 
    chunk_type: if is outside or inside entity
    """    
    #true chunk
    chunk_ids = []
    chunk_type = [0]
    chunk_id = 0

    for i, row in pdf.iterrows():
        #true labels chunk
        if row.lab == 'O':
            if chunk_type[-1] != 0:
                chunk_id +=1
            chunk_ids.append(chunk_id)
            chunk_type.append(0)
        elif row.lab[0]=="B":
            chunk_id +=1
            chunk_ids.append(chunk_id)
            chunk_type.append(1)

        elif row.lab[0] == "I":
            chunk_ids.append(chunk_id)
            chunk_type.append(1)
            
    return chunk_ids, chunk_type[1:]

def conll2brat(pdf):
    assert pdf.columns.tolist() == ['tok', 'lab'], "Columns name should be 'tok', 'lab'"
    pdf = pdf.copy()
    text = " ".join(pdf.tok)
    
    #get chunk id
    chunk_ids, chunk_type = get_conll_chunk_id(pdf)
    pdf["chunk_id1"] = chunk_ids
    pdf["chunk_type1"] = chunk_type
    
    #compute span taking account of \n and " "
    pdf = (pdf
           .assign(toklen =  lambda x:(x.tok.str.len()))
           .assign(endspan = lambda x:(x.toklen  + 1).cumsum()-1)
           .assign(startspan = lambda x:x.endspan-x.toklen)
          )
    
    #to brat format
    pdf = (pdf.loc[lambda x:x.chunk_type1==1]
           .groupby('chunk_id1')
           .apply(lambda x: x.lab.tolist()[0].split('-')[-1] +" "+str(x.startspan.values[0]) +" "+str(x.endspan.values[-1])+"\t"+ " ".join(x.tok.tolist()))
           )
    
    if len(pdf)>0:
        pdf = pdf.str.split('\t', expand=True).reset_index(drop=True)

        pdf.columns = ['ent', 'txt']

        #check
        check_brat_file(text, pdf)
    else:
        pdf = pd.DataFrame(columns=['ent', 'txt'])
    return text, pdf


def reformat_ann(ann):
    """Entities names must follow be defined ngrams separated by underscore, the higher number of ngrams the higher in the hierarchy of the tree"""
    if not ann.empty:
        spans = (pd.DataFrame([[t.split()[i] for i in [0,1, len(t.split())-1]] for t in ann.ent.tolist()])
                 .rename(columns={0:'ent_name', 1:"start", 2:"stop"})
                 .assign(ent_name = lambda x:x.ent_name.str.upper())
                 .assign(start = lambda x:x.start.astype('int'))
                 .assign(stop = lambda x:x.stop.astype('int'))
                 .assign(hierarchy_depth = lambda x:x.ent_name.apply(lambda x:len(x.split("_"))))
                )
        assert not spans.loc[:,["start", "stop", "hierarchy_depth"]].duplicated().any(), "need hierarchy"
        spans = (spans.sort_values(["start", "stop", "hierarchy_depth"], ascending=[True, False, False])
                 .drop("hierarchy_depth", axis = 1)
                 .reset_index(drop=True)
                )
    
    else:
        spans = pd.DataFrame(columns = ["ent_name", "start", "stop"])
    
    return spans

def brat2rnng(tokens, ann):
    tokens_starts = np.array([0] + [len(t) + 1 for t in tokens]).cumsum()
    tokens_stops = tokens_starts[1:] -1
    tree = ["[" + ROOT_LABEL]

    state = 1

    for i, tok in enumerate(tokens):
        open_ent = ann.loc[lambda x:x.start == tokens_starts[i]]
        close_ent = ann.loc[lambda x:x.stop == tokens_stops[i]]

        for i, row in open_ent.iterrows():
            tree.append("["+row.ent_name)
            state +=1

        tree.append(tok)

        for i, row in close_ent.iterrows():
            tree.append("]")
            state -=1



    tree.append(" ]"*state)
    
    return " ".join(tree)


def new_brat2rnng(text, ann):

    tree = ["[" + ROOT_LABEL]
    state = 1

    for _, tok in text.iterrows():
        open_ent = ann.loc[lambda x:x.start == tok.start]
        close_ent = ann.loc[lambda x:x.stop == tok.stop]

        for i, row in open_ent.iterrows():
            tree.append("["+row.ent_name)
            state +=1

        tree.append(tok.tok)

        for i, row in close_ent.iterrows():
            tree.append("]")
            state -=1



    tree.append(" ]"*state)    
    tree = " ".join(tree)
    
    return tree

def rnng2brat(tree_string):
    action_stack = []
    brat_ann = []
    span = 0
    #iterate through actions and tokens
    for tok in tree_string.split():
        if tok[0] == "[":
            action_stack.append([tok[1:], span])
        elif tok[0] == "]":
            action_stack[-1].append(span-1)
            brat_ann.append(action_stack[-1])
            action_stack.pop()
        else:
            span += len(tok) + 1

    brat_ann = (pd.DataFrame(brat_ann, columns=['ent_name', 'start', 'stop'])
            .assign(hierarchy_depth = lambda x:x.ent_name.apply(lambda x:len(x.split("_"))))
            .sort_values(["start", "stop", "hierarchy_depth"], ascending=[True, False, False]).drop("hierarchy_depth", axis = 1)
            .loc[lambda x:x.ent_name!=ROOT_LABEL]
            .reset_index(drop=True)
           )
    return brat_ann


# def get_brat(fn):
#     """Load and transform brat file to pandas dataframe"""
#     with open(fn, "r") as h:
#         ann = h.read()
#     with open(fn[:-3]+"txt", "r") as h:
#         txt = h.read()
        
#     if len(ann)>0:
#         ann = pd.read_csv(StringIO(ann), sep ="\t", header = None)
#         ann.columns = ["id", "ent", "txt"]
#     else:
#         ann = None
    

#     return ann, txt

# def reformat_ann(ann):
#     ann = ann.loc[ann.id.str[0] == "T"]
    
#     spans = (pd.DataFrame([[t.split()[i] for i in [0,1, len(t.split())-1]] for t in ann.ent.tolist()])
#              .rename(columns={0:'ent_name', 1:"start", 2:"stop"})
#              .replace({"ent_name":replace_dict
#                       }
#                      )
#              .assign(ent_name = lambda x:x.ent_name.str.upper()
#                     )
#              .assign(start = lambda x:x.start.astype('int'))
#              .assign(stop = lambda x:x.stop.astype('int'))
#             )
    
#     return spans.sort_values(["start", "stop"])

# def transform_brat2rnng(fn, replace_dict):
#     ann, txt = get_brat(fn)

#     if ann is not None:
#         ann = reformat_ann(ann)
        
#     return txt, ann

# replace_dict = {"prescription_medicament_start":"DRUG_BLOB",
#                                        "prescription_medicament_continue":"DRUG_BLOB",
#                                        'prescription_medicament_decrease':'DRUG_BLOB',
#                                        'prescription_medicament_increase':'DRUG_BLOB',
#                                        'prescription_medicament_stop':'DRUG_BLOB',
#                                        'prescription_medicament_unique':'DRUG_BLOB',
#                                        'prescription_ordonnance_continue':'ORDO_BLOB',
#                                        'prescription_ordonnance_start':'ORDO_BLOB',
#                                        'prescription_ordonnance_stop':'ORDO_BLOB',
#                                        'prescription_ordonnance_switch':'ORDO_BLOB',
#                                        'prescription_ordonnance_unique':'ORDO_BLOB',
#                'start-stop':"START_STOP"}

# corpus = {}
# for fi, filename in enumerate(fns):
#     text, anno = transform_brat2rnng(filename, replace_dict)
#     break
###############################CONLL 2 RNNG ###################################################
def to_nltk_tree(node):
    """Transform a pytext RNNG annotation to a NLTK tree, usefull for pretty-printing
    
    Parameters:
    node (Annotation): a pytext Annotation data structure

    Returns:
    tree:Returning a NLTK tree
    """
    if node.children is not None:
        return Tree(node.label, [to_nltk_tree(child) for child in node.children])
    else:
        return node.label
    
    
# def to_nltk_tree(s):
#     s = re.sub('\[', '(', s)
#     s = re.sub('\]', ')', s)
#     s = Tree.fromstring(s)
#     return s
    

def get_file(fn, kept_columns = ['tok', 'ent', 'ev', 'drug_blob', 'ordo_blob'] ):
    """Load and transform Conll file to pandas dataframe"""
    pdf = pd.read_csv(fn, sep="\t", header=None, keep_default_na =False)
    pdf.columns = ['tok', 'ent', 'ent_attr', 'ev', 'event_attr', 'drug_blob',
           'drug_blob_attr', 'ordo_blob', 'ordo_blob_attr']

    pdf = pdf.loc[:,kept_columns]

    return pdf

def repair_conll(pdf):
    """Check if any labelled doesn't start by I and if the case change it to B. Also remove - from labels"""
    
    #repair I- instead of B-
    for i, row in pdf.iterrows():
        if row.ent[0] == 'I':
            if i == 0:
                pdf.loc[i].ent = "B" + pdf.loc[i].ent[1:]   
            elif pdf.loc[i-1].ent == 'O':
                pdf.loc[i].ent = "B" + pdf.loc[i].ent[1:]   

        if row.ev[0] == 'I':
            if i == 0:
                pdf.loc[i].ev = "B" + pdf.loc[i].ev[1:]   
            elif pdf.loc[i-1].ev == 'O':
                pdf.loc[i].ev = "B" + pdf.loc[i].ev[1:]   

        if row.drug_blob[0] == 'I':
            if i == 0:
                pdf.loc[i].drug_blob = "B" + pdf.loc[i].drug_blob[1:]   
            elif pdf.loc[i-1].drug_blob == 'O':
                pdf.loc[i].drug_blob = "B" + pdf.loc[i].drug_blob[1:]   
                
        if row.ordo_blob[0] == 'I':
            if i == 0:
                pdf.loc[i].ordo_blob = "B" + pdf.loc[i].ordo_blob[1:]   
            elif pdf.loc[i-1].ordo_blob == 'O':
                pdf.loc[i].ordo_blob = "B" + pdf.loc[i].ordo_blob[1:]       #repair label with -
                
    pdf = pdf.assign(ev = pdf.ev.str[:2]  + pdf.ev.str[2:].str.replace('-', '_'))
    
    #repair [ ]
    pdf = pdf.assign(tok = pdf.tok.str.replace("[", "{").str.replace("]", "}"))
    return pdf

def split_sent(pdf):
    """split pandas dataframe in dict sent_num: pandas dataframe for each sentence"""
    pdf = pdf.assign(sent_id = (pdf=='').all(1).cumsum()).loc[~(pdf=='').all(1)]
    return {k: group.drop('sent_id', axis=1).reset_index(drop=True) for k, group in pdf.groupby('sent_id')}

def to_rnnpg_structure(pdf):
    """Transform conll to rnng format for pytext
    Parameters:
    pdf (pandas dataframe): a sentence in conll format with 1 line per token to
    
    Returns:
        * the raw text separated by space
        * the parsed sentence in the rnng pytext format
        * and the parsed sentence as an nltk tree structure
        """
    sent_acc = []
    tree = []

    last_row = len(pdf)
    state = 0
    inside_drug_blob = False
    fake_drug_blob = False
    inside_ordo_blob = False
    fake_ordo_blob = False
    fake_ordo_blob2 = False

    #for each token
    for ri, row in pdf.iterrows():
        #open constituent if
        if ri == 0:
            tree.append("[IN:SENTENCE")
            state += 1
        
        #open slot if B-Ordo_Blob
        if row.ordo_blob == "B-Ordo_Blob":                
            tree.append("[SL:ORDO_BLOB")
            state += 1
            inside_ordo_blob = True

        #open intent in B-Drug
        if row.drug_blob == "B-Drug_Blob":    
            #append fake ordo_blob
            if not inside_ordo_blob:
                tree.append("[SL:ORDO_BLOB")
                state += 1
                inside_ordo_blob = True
                fake_ordo_blob2 = True
            #append drug blob
            tree.append("[IN:" + row.drug_blob.split('-')[-1].upper())
            state += 1
            inside_drug_blob = True

        #open slot if start of entity
        if row.ent[0] == "B":
            #append fake ordo blob
            if not inside_ordo_blob:
                tree.append("[SL:ORDO_BLOB")
                state += 1
                inside_ordo_blob = True
                fake_ordo_blob = True
            #append fake ordo blob
            if not inside_drug_blob:
                tree.append("[IN:DRUG_BLOB")
                state += 1
                inside_drug_blob = True
                fake_drug_blob = True
                
            #append ent
            tree.append("[SL:" + row.ent.split('-')[-1].upper())
            state += 1

        #open slot + int if start of event
        if row.ev[0] == "B":
            #append fake ordo blob
            if not inside_ordo_blob:
                tree.append("[SL:ORDO_BLOB")
                state += 1
                inside_ordo_blob = True
                fake_ordo_blob = True
                
            #append fake ordo blob
            if not inside_drug_blob:
                tree.append("[IN:DRUG_BLOB")
                state += 1
                inside_drug_blob = True
                fake_drug_blob = True
            
            #append ev
            tree.append("[SL:" + row.ev.split('-')[-1].upper())
            tree.append("[IN:" + row.ev.split('-')[-1].upper())
            state += 1
            state += 1

        #start append token if all int and slot have been open for this token
        tree.append(row.tok)
        sent_acc.append(row.tok)

        #close
        #except last tok
        if ri < last_row - 1:
            next_row = pdf.loc[ri+1]
            #close if inside an entity
            if ( row.ent.split('-')[-1] != 'O'):
                #and the next token is not of that entity
                if ('I-' + row.ent.split('-')[-1] != next_row.ent):
                    tree.append(" ]")
                    state -= 1
                    #close fake drug blob
                    if fake_drug_blob:
                        tree.append(" ]")
                        state -= 1
                        fake_drug_blob = False
                        inside_drug_blob = False
                    #close fake ordo blob
                    if fake_ordo_blob:
                        tree.append(" ]")
                        state -= 1
                        fake_ordo_blob = False
                        inside_ordo_blob = False
                        
            #close if inside of event
            if ( row.ev.split('-')[-1] != 'O'):
                #and the next token is not of that evebt
                if ('I-' + row.ev.split('-')[-1] != next_row.ev):
                    tree.append(" ]")
                    tree.append(" ]")
                    state -= 1
                    state -= 1
                    #close fake drug blob
                    if fake_drug_blob:
                        tree.append(" ]")
                        state -= 1
                        fake_drug_blob = False
                        inside_drug_blob = False
                    #close fake ordo blob
                    if fake_ordo_blob:
                        tree.append(" ]")
                        state -= 1
                        fake_ordo_blob = False
                        inside_ordo_blob = False
                        
            #if inside drug blob
            if row.drug_blob.split('-')[-1] != 'O':
                #close if end of drug blob
                if 'I-' + row.drug_blob.split('-')[-1] != next_row.drug_blob:
                    tree.append(" ]")
                    state -= 1
                    if fake_ordo_blob2:
                        tree.append(" ]")
                        state -= 1 #for fake ordo blob
                        fake_ordo_blob2 = False
                        inside_ordo_blob = False
                    inside_drug_blob = False

            #if inside ordo blob
            if row.ordo_blob.split('-')[-1] != 'O':
                #close if end of ordo blob
                if 'I-' + row.ordo_blob.split('-')[-1] != next_row.ordo_blob:
                    tree.append(" ]")
                    state -= 1
                    inside_ordo_blob = False
            else:
                #just add a token
                pass

        #close if last tok
        else:
            #as many times as deep in the hierarchy
            tree.append(" ]" * state)
            state -= 1 * state

    assert state == 0

    tree = " ".join(tree)
    sent_acc = " ".join(sent_acc)
    
    #test if valid
    nltk_tree = string2tree(tree)
    
    return sent_acc, tree, nltk_tree

def string2tree(strg , valid_actions_conf):
    root = Annotation(strg, valid_actions_conf = valid_actions_conf).root
    nltk_tree = to_nltk_tree(root)
    return nltk_tree

def rnng2conll(strg, column_param = {"ent":["drug", "dose", "class", "freq", "duree", "condition",  "route"],
                                     "ev":["start", "switch", "stop", "continue", "increase", "decrease", "start_stop"],
                                     "ordo_drug_blob":["ordo_drug_blob"],
                                     "drug_blob":["drug_blob"]
                                    }
              ):
    """Transform rnng format to conll format
    Parameters:
    strg (str): a rnng string with actions and tokens

    Returns:
    pdf: a pandas dataframe in the conll format
    """

    #init accumutator
    conll_sent = []
    state = {
            "tok":"",
            "ent" : "O",
             "ev" : "O",
             "drug_blob" : "O",
             "ordo_drug_blob" : "O",
            "last_open" : []
            }
    last_state = state
    #transform to get index of column parameter for each entity type
    slot2column = {k:v for sub in [{t:k for t in v} for k,v in column_param.items()] for k,v in sub.items()}
    slot2column['sentence'] = 'sentence'

    #iterate through actions and tokens
    for tok in strg.split():
        #update state to open a entity if action open
        if tok[0] == "[":
            state['last_open'].append(tok[1:])
            col_ind = slot2column[tok[1:].lower()]
            state[col_ind] = "B-"+tok[1:].lower()
            
            last_state['tok'] = tok

        #update state to close last openned entity if action close
        elif tok[0] == "]":
            last_open = state['last_open'].pop().lower()
            col_ind = slot2column[last_open]
            state[col_ind] = "O"
            last_state['tok'] = tok
        #update state if is token  and append state to accumulator
        else:
            #change B to I if previous row for given columns is of the same type
            for k in column_param.keys():
                #if current state is opened entity
                if state[k][0] == "B":
                    #if last row is the same
                    if last_state[k] == state[k]:
                        #if last open entity is the same (contiguous entity)
                        if state['last_open'][-1] == k:
                            #need to check if entity wasn't just closed
                            if last_state["tok"][0] not in ['[', ']']:
                                state[k] = "I" + state[k][1:]
                        #else okay to go to I
                        else:
                            if last_state["tok"][0] not in ['[']:
                                state[k] = "I" + state[k][1:]

            state["tok"] = tok
            conll_sent.append(deepcopy(state))
            last_state = deepcopy(state)
            
            


    #to pandas
    conll_sent = pd.DataFrame(conll_sent).loc[:,['tok', 'ent', 'ev', 'drug_blob', 'ordo_drug_blob']]

    return conll_sent