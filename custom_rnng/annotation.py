#!/usr/bin/env python3
# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
import re
from typing import Any, List, Tuple, Union, Dict
from copy import deepcopy

OPEN = "["
CLOSE = "]"
ESCAPE = "\\"
INTENT_PREFIX = "IN:"
SLOT_PREFIX = "SL:"
COMBINATION_INTENT_LABEL = INTENT_PREFIX + "COMBINE"
COMBINATION_SLOT_LABEL = SLOT_PREFIX + "COMBINE"

       
INVALID_TREE_STR = "[IN:INVALID_TREE placeholder]"
SEQLOGICAL_LOTV_TOKEN = "0"

SHIFT = "SHIFT"
REDUCE = "REDUCE"
ROOT_LABEL = "SENTENCE"
PAD_LABEL = "PAD_LABEL"



def escape_brackets(string: str) -> str:
    return re.sub(rf"([\{OPEN}\{CLOSE}\{ESCAPE}])", rf"\{ESCAPE}\1", string)


"""
A data structure for a general slot annotation:
Each Node has the type GeneralSlot or Token, a pointer to its parent, and
a list of its children. Token's children == None

Annotation.validate_tree() will check for valid nesting which is define by a set of valid children for each parent.

However, this class is intended not for validation but for annotator agreement
calculations.
"""

def get_valid_from_stack(top_action_stack):
    #valid actions are those allowed from last open consituents
    ## and with maximum not reached yet
    actions_set = [k for k,v in top_action_stack.items() if v["max"] > 0]
    #todo:actions_set if sum of min SHIFT >= len(buffer)
    
    #SHIFT is allowed if mamimum SHIFT not reached 
    ##and during inference, if buffer len is <=1, if all NON SHIFT minimum are reached
#     if buffer_len and buffer_len <=1:
#         if all([v["min"] <= 0 for k,v in top_action_stack.items() if k!=SHIFT]):
#             actions_set.add(SHIFT)
#         else:
#             actions_set.remove(SHIFT)

    #REDUCE is allowed if all miminum are reached
    if all([v["min"] <= 0 for k,v in top_action_stack.items()]):
        actions_set += [REDUCE]
        
    return set(actions_set)

def update_valid_actions_stack(action, valid_actions_stack, valid_actions_conf):
    valid_actions_conf = deepcopy(valid_actions_conf)
    
    if action == SHIFT:
        valid_actions_stack[-1][SHIFT]['min'] -=1
        valid_actions_stack[-1][SHIFT]['max'] -=1
    elif action == REDUCE:
        valid_actions_stack.pop()
    else:
        valid_actions_stack[-1][action]['min'] -=1
        valid_actions_stack[-1][action]['max'] -=1
#         if (ANY_LABEL[0] in valid_actions_stack[-1].keys()) & (action in ANY_LABEL[1]):
#             valid_actions_stack[-1][ANY_LABEL[0]]['min'] -=1
#             valid_actions_stack[-1][ANY_LABEL[0]]['max'] -=1
#         if (ANY_DRUG_RULE[0] in valid_actions_stack[-1].keys()) & (action in ANY_DRUG_RULE[1]):
#             valid_actions_stack[-1][ANY_DRUG_RULE[0]]['min'] -=1
#             valid_actions_stack[-1][ANY_DRUG_RULE[0]]['max'] -=1
#         if (ANY_ATTR_RULE[0] in valid_actions_stack[-1].keys()) & (action in ANY_ATTR_RULE[1]):
#             valid_actions_stack[-1][ANY_ATTR_RULE[0]]['min'] -=1
#             valid_actions_stack[-1][ANY_ATTR_RULE[0]]['max'] -=1
#         if (ANY_BLOB_RULE[0] in valid_actions_stack[-1].keys()) & (action in ANY_BLOB_RULE[1]):
#             valid_actions_stack[-1][ANY_BLOB_RULE[0]]['min'] -=1
#             valid_actions_stack[-1][ANY_BLOB_RULE[0]]['max'] -=1
            
        
        valid_actions_stack.append(valid_actions_conf[action])
        
        

class CustomAnnotation:
    def __init__(
        self,
        annotation_string: str,
        utterance: str = "",
        brackets: str = OPEN + CLOSE,
        combination_labels: bool = True,
        add_dict_feat: bool = False,
        accept_flat_intents_slots: bool = False,
        valid_actions_conf: Dict = {}
    ) -> None:
        super(CustomAnnotation, self).__init__()
        self.OPEN = brackets[0]
        self.CLOSE = brackets[1]
        self.combination_labels = combination_labels

        # Expected annotation_string (tab-separated):
        # intent, slots, utterance, sparse_feat, seqlogical
        # OR only seqlogical
        parts = annotation_string.rstrip().split("\t")
        if len(parts) == 5:
            [_, _, utterance, sparse_feat, self.seqlogical] = parts
        elif len(parts) == 1:
            [self.seqlogical] = parts
        else:
            raise ValueError("Cannot parse annotation_string")

        self.tree = Tree(
            self.build_tree(accept_flat_intents_slots), combination_labels, utterance, valid_actions_conf
        )
        self.root: GeneralSlot = self.tree.root

    def build_tree(self, accept_flat_intents_slots: bool = False):


        
#         root = GeneralSlot(ROOT_LABEL)
#         node_stack: List[Any] = [root]
        node_stack: List[Any] = []
        curr_chars: List[str] = []
        token_count = 0
        expecting_label = False
        it = iter(self.seqlogical)

        while True:
            char = next(it, None)
            if char is None:
                break
                
            
            if char.isspace() or char in (OPEN, CLOSE):
                #build words if curr_chars not empty
                if curr_chars:
                    word = "".join(curr_chars)
                    curr_chars = []
                    #get last parent
                    if word == ROOT_LABEL:
                        root = GeneralSlot(word)
                        node_stack.append(root)
                        expecting_label = False
                    else:
                        parent = node_stack[-1]
                        #expecting a action label
                        if expecting_label:
                            #constraint on label
                            node = GeneralSlot(word)
                            node_stack.append(node)
                            expecting_label = False


                        #expecting a token
                        else:
                            node = Token(word, token_count)
                            token_count += 1

                        #append current node to parent
                        parent.children.append(node)

                        #set current parent to current node
                        node.parent = parent


                    
                
                if char in (OPEN, CLOSE):
                    if expecting_label:
                        raise ValueError("Invalid tree. No label found after '['.")
                    # if char is open or close we expect a label
                    if char == OPEN:
                        expecting_label = True
                    
                    #if close go up in hierarchy
                    else:
                        node_stack.pop()
                        
                        
            else:
                # build words if not open/close/space
                if char == ESCAPE:
                    char = next(it, None)
                    if char not in (OPEN, CLOSE, ESCAPE):
                        raise ValueError(
                            f"Escape '{ESCAPE}' followed by none of '{OPEN}', "
                            f"'{CLOSE}', or '{ESCAPE}'."
                        )
                curr_chars.append(char)

                
        if len(node_stack) != 0:
            raise ValueError("Invalid tree.")

        #add combination label if root has more than 1 child
#         if len(root.children) > 1 and self.combination_labels:
#             assert False, "Root should have one child"
#             comb_intent = Intent(COMBINATION_INTENT_LABEL)
#             node_stack.insert(1, comb_intent)
#             for child in root.children:
#                 if type(child) == Intent:
#                     comb_slot = Slot(COMBINATION_SLOT_LABEL)
#                     comb_slot.parent = comb_intent
#                     comb_slot.children.append(child)
#                     comb_intent.children.append(comb_slot)
#                     child.parent = comb_slot
#                 else:
#                     child.parent = comb_intent
#                     comb_intent.children.append(child)
#             comb_intent.parent = root
#             root.children = [comb_intent]

        return root

    def __str__(self):
        """
        A tab-indented version of the tree.
        strip() removes an extra final newline added during recursion
        """
        return self.tree.__str__()

    def __eq__(self, other):
        return self.tree == other.tree


class Node:
    def __init__(self, label):
        self.label = label  # The name of the intent, slot, or token
        self.children = []  # the children of this node (Intent, Slot, or Token)
        self.parent = None

    def list_ancestors(self):
        ancestors = []
        if self.parent:
            if self.parent.label != ROOT_LABEL:
                ancestors.append(self.parent)
                ancestors += self.parent.list_ancestors()
        return ancestors

    def validate_node(self):
        if self.children:
            for child in self.children:
                child.validate_node()

    # Returns all tokens in the span covered by this node
    def list_tokens(self):
        tokens = []
        if self.children:
            for child in self.children:
                if type(child) == Token:
                    tokens.append(child.label)
                else:
                    tokens += child.list_tokens()
        return tokens

    def get_token_span(self):
        """
        0 indexed
        Like array slicing: For the first 3 tokens, returns 0, 3
        """
        indices = self.get_token_indices()
        if len(indices) > 0:
            return min(indices), max(indices) + 1
        else:
            return None

    def get_token_indices(self):
        indices = []
        if self.children:
            for child in self.children:
                if type(child) == Token:
                    indices.append(child.index)
                else:
                    indices += child.get_token_indices()
        return indices

    def list_nonTerminals(self):
        """
        Returns all Intent and Slot nodes subordinate to this node
        """
        non_terminals = []
        for child in self.children:
            if child.label != ROOT_LABEL and type(child) != Token:
                non_terminals.append(child)
                non_terminals += child.list_nonTerminals()
        return non_terminals

    def list_terminals(self):
        """
        Returns all Token nodes
        """
        terminals = []
        for child in self.children:
            if type(child) == Token:
                terminals.append(child)
            else:
                terminals += child.list_terminals()
        return terminals

    def get_info(self):
        if type(self) == Token:
            return Token_Info(self)
        return Node_Info(self)

    def flat_str(self):
        string = ""
        if type(self) == GeneralSlot and self.label != ROOT_LABEL:
            string = OPEN
        if self.label != ROOT_LABEL:
            string += escape_brackets(str(self.label)) + " "
        if self.children:
            for child in self.children:
                string += child.flat_str()
        if type(self) == GeneralSlot and self.label != ROOT_LABEL:
            string += CLOSE + " "
        return string

    def children_flat_str_spans(self):
        string = str(self.get_token_span()) + ":"
        if self.children:
            for child in self.children:
                string += child.flat_str()
        return string

    def __str__(self):
        string = self._recursive_str("", "")
        return string

    def _recursive_str(self, string, spacer):
        string = spacer + str(self.label) + "\n"
        spacer += "\t"

        if self.children:
            for child in self.children:
                string += child._recursive_str(string, spacer)
        return string

    def __eq__(self, other):
        return self.label == other.label and self.children == other.children

class GeneralSlot(Node):
    def __init__(self, label):
        super().__init__(label)

    def validate_node(self, constraint):
        if self.label not in constraint:
            raise TypeError(
                " {} is not in: {} or max reached".format(self.label, self.parent.label)
            )
            



    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        return self.label == other.label and self.children == other.children


class Token(Node):
    def __init__(self, label, index):
        super().__init__(label)
        self.index = index
        self.children = None

    def validate_node(self, constraint):
        if self.children is not None:
            raise TypeError(
                "A token node is terminal and should not \
                    have children: "
                + self.label
                + " "
                + str(self.children)
            )
        if SHIFT not in constraint:
            raise TypeError(
                " {} is not in: {}".format(self.label, self.parent.label)
            )
            


    def remove(self):
        """
        Removes this token from the tree
        """
        self.parent.children.remove(self)
        self.parent = None

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        return self.label == other.label and self.index == other.index


class Token_Info:
    """
    This class extracts the essential information for a token for use in rules.
    """

    def __init__(self, node):

        self.token_word = node.label

        self.parent_label = self.get_parent(node)

        self.ancestors = [a.label for a in node.list_ancestors()]

        self.prior_token = None
        prior = node.get_prior_token()
        if prior:
            self.prior_token = prior.label

        self.next_token = None
        next_token = node.get_next_token()
        if next_token:
            self.next_token = next_token.label

    def get_parent(self, node):
        if node.parent and node.parent.label != ROOT_LABEL:
            return node.parent.label
        return None

    def __str__(self):
        result = []
        result.append("Token Info:")
        result.append("Token Word: " + self.token_word)
        result.append("Previous Token: " + str(self.prior_token))
        result.append("Next Token: " + str(self.next_token))
        result.append("Parent: " + str(self.parent_label))
        result.append("Ancestors: " + ", ".join(self.ancestors))
        return "\n".join(result)


class Node_Info:
    """
    This class extracts the essential information for a mode, for use in rules.
    """

    def __init__(self, node):
        self.label = node.label
        # This is all descendent tokens, not just immediate children tokens
        self.tokens = node.list_tokens()
        # If no parent, None
        self.parent_label = self.get_parent(node)

        # only look at slot or intent children
        self.children = []
        for a in node.children:
            if type(self) == GeneralSlot and self.label != ROOT_LABEL:
                self.children.append(a.label)

        self.token_indices = node.get_token_indices()
        self.ancestors = [a.label for a in node.list_ancestors()]
        # This is only non-temrinal descendents. Did you want tokens too?
        self.descendents = [d.label for d in node.list_nonTerminals()]
        self.prior_token = None
        prior = node.get_prior_token()
        if prior:
            self.prior_token = prior.label
        if type(node) == Token:
            self.label_type = "SHIFT"
        elif type(node) == GeneralSlot:
            self.label_type = "SLOT"

        # same span as parent
        self.same_span = self.get_same_span(node)

    def get_same_span(self, node):
        if node.parent:
            if set(node.parent.list_tokens()) == set(node.list_tokens()):
                return True
        return False

    def get_parent(self, node):
        if node.parent and node.parent.label != ROOT_LABEL:
            return node.parent.label
        return None

    def __str__(self):
        result = []
        result.append("Info:")
        result.append("Label: " + self.label)
        result.append("Tokens: " + " ".join(self.tokens))
        result.append(
            "Token Indicies: " + ", ".join([str(i) for i in self.token_indices])
        )
        result.append("Prior Token: " + str(self.prior_token))
        result.append("Parent: " + str(self.parent_label))
        result.append("Children: " + ", ".join(self.children))
        result.append("Ancestors: " + ", ".join(self.ancestors))
        result.append("Descendents: " + ", ".join(self.descendents))
        result.append("Label Type: " + str(self.label_type))
        result.append("Same Span: " + str(self.same_span))
        return "\n".join(result)


class Tree:
    def __init__(
        self, root: GeneralSlot, combination_labels: bool, utterance: str = "", valid_actions_conf: dict = {}
    ) -> None:
        self.root = root
        self.combination_labels = combination_labels
        self.valid_actions_conf = valid_actions_conf
        #new constraints
        self.valid_actions_stack = [{ROOT_LABEL:{"min":1, "max":1}}]
        
        
        try:
            self.validate_tree()
        except ValueError as v:
            raise ValueError(
                "Tree validation failed: {}. \n".format(v)
                + "Utterance is: {}".format(utterance)
            )

    def validate_tree(self):
        """
        This is a method for checking that roots/intents/slots are
        nested correctly.
        Root( Intent( Slot( Intent( Slot, etc.) ) ) )
        """

        try:
#             if self.combination_labels and not len(self.root.children) == 1:
#                 raise ValueError(
#                     """Root should always have one child and not {}.
#                     Look into {} and {}""".format(
#                         len(self.root.children),
#                         COMBINATION_INTENT_LABEL,
#                         COMBINATION_SLOT_LABEL,
#                     )
#                 )
            self.recursive_validation(self.root)
        except TypeError as t:
            raise ValueError(
                "Failed validation for {}".format(self.root) + "\n" + str(t)
            )

    def recursive_validation(self, node):
        if not isinstance(node, Token):
            #OPEN
            #valid
            valid_actions_set = get_valid_from_stack(self.valid_actions_stack[-1])
            node.validate_node(valid_actions_set)
            #update
            update_valid_actions_stack(node.label, self.valid_actions_stack, self.valid_actions_conf)
            for child in node.children:
                self.recursive_validation(child)
            
            #REDUCE
            valid_actions_set = get_valid_from_stack(self.valid_actions_stack[-1])
            if REDUCE in valid_actions_set:
                #update
                update_valid_actions_stack(REDUCE, self.valid_actions_stack, self.valid_actions_conf)
            else:
                raise TypeError(
                    "cannot {} because: {} superior to 0".format(REDUCE, [k for k,v in self.valid_actions_stack[-1].items() if v["min"] > 0])
                )

        else:

            #SHIFT
            #valid
            valid_actions_set = get_valid_from_stack(self.valid_actions_stack[-1])
            node.validate_node(valid_actions_set)
            #update
            update_valid_actions_stack(SHIFT, self.valid_actions_stack, self.valid_actions_conf)


            

    def print_tree(self):
        print(self.flat_str())

    def flat_str(self):
        return self.root.flat_str()

    def lotv_str(self):
        """
        LOTV -- Limited Output Token Vocabulary
        We map the terminal tokens in the input to a constant output
        (SEQLOGICAL_LOTV_TOKEN) to make the parsing task easier for models where
        the decoding is decoupled from the input (e.g. seq2seq). This way,
        the model can focus on learning to predict the parse tree,
        rather than waste effort learning to replicate terminal tokens.
        """
        flat_str = self.root.flat_str().split(" ")
        all_tokens = self.list_tokens()
        return " ".join(
            [
                token if token not in all_tokens else SEQLOGICAL_LOTV_TOKEN
                for token in flat_str
            ]
        )

    def list_tokens(self):
        return self.root.list_tokens()

    def depth(self):
        # note that this calculation includes Root as part of the tree
        return self._depth(self.root)

    def _depth(self, n):
        if n.children:
            depths = []
            for c in n.children:
                depths.append(self._depth(c))
            return max(depths) + 1
        else:
            return 0

    def to_actions(self):
        actions = [self.root.label]
        self._to_actions(self.root, actions)
        actions.append(REDUCE)
        return actions

    def _to_actions(self, node: Node, actions: List[str]):
        if type(node) == Token:
            actions.append(SHIFT)
            return
        
        for child in node.children:
            if not type(child) is Token:
                actions.append(child.label)
                self._to_actions(child, actions)
                actions.append(REDUCE)
            else:
                self._to_actions(child, actions)

    def __str__(self):
        """
        A tab-indented version of the tree.
        strip() removes an extra final newline added during recursion
        """
        return self.root.__str__().strip()

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        return self.root == other.root


class TreeBuilder:
    def __init__(self, combination_labels: bool = True) -> None:
        self.combination_labels = combination_labels
        self.node_stack = []
        self.token_count = 0
        self.finalzed = False

    def update_tree(self, action, label):
        assert not self.finalzed, "Cannot update tree since it's finalized"
        if action == REDUCE:
            self.node_stack.pop()
               
        elif action == SHIFT:
            token = Token(label, self.token_count)
            self.token_count += 1
            token.parent = self.node_stack[-1]
            self.node_stack[-1].children.append(token)
        elif action == ROOT_LABEL:
            self.root = GeneralSlot(ROOT_LABEL)
            self.node_stack.append(self.root)
        else:
            self.node_stack.append(GeneralSlot(label))
            self.node_stack[-1].parent = self.node_stack[-2]
            self.node_stack[-2].children.append(self.node_stack[-1])
            

#         else:
#             raise ValueError("Don't understand action %s" % (action))
            


    def finalize_tree(self, valid_actions_conf):
        return Tree(self.root, self.combination_labels, valid_actions_conf = valid_actions_conf)


