# medRNNG

Code for the model used in 'Learning the grammar of prescription: recurrent neural network grammars for medication information extraction in clinical texts' (https://arxiv.org/abs/2004.11622), a modified version of that of Gupta et al. (https://github.com/facebookresearch/pytext/tree/master/pytext/models/semantic_parsers/rnng)

### Install pytext
pip install pytext-nlp==0.1.5

### Usage
pytext --include custom_rnng train < config/best_config.json